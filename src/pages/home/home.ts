import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  pushPage() :void {
    this.navCtrl.push(ContactPage)
  }

  setPage() :void {
    this.navCtrl.setRoot(ContactPage)
    //ao definirmos a pagina contato como root ao cliclarmos em voltar, vai da um erro porque a pagina não está empilhada.
  }

}
